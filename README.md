# **IMPORTANT NOTICE**

### _**Make sure to read the followwing lines before trying to get this program to work**_

This code has been writen for the Raspeberry Pi Model 4, so it take advantage of the extra UART interfaces provided. Here we are using UART2 on GPIO 0 and 1, make sure you have added the correct dtoverlay command in `/boot/config.txt` file.

Also, make sure to have installed pigpio library :

http://abyz.me.uk/rpi/pigpio/index.html

### **The dtoverlay command added :**

```bash
[UART]
# Enable UART 2 on GPIO 0-3
dtoverlay=uart2
```
See : https://www.raspberrypi.org/documentation/configuration/uart.md

