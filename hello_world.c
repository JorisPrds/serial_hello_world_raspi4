/*****************************************************

* hello_world.c

* Simple "Hello World" program to test UART 
* functionality of the pigpio library

* JorisPrds, July 29, 2020

*****************************************************/

/*

* IMPORTANT :   This code has been writen for the
*               pi4, so it take advantage of the
*               extra UART interfaces provided.
*               Here we are using UART2 on GPIO
*               0 and 1, make sure you have added
*               the correct dtoverlay command in
*               /boot/config.txt file.

* See : https://www.raspberrypi.org/documentation/configuration/uart.md

*/

// Libraries inclusions
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <pigpio.h>

// Main Entry Point
int main (void)
{
    // LOCAL VARIABLES
    bool printed = false;
    int serHandle;
    
    // Check pigpio initialization
    if (gpioInitialise() < 0) 
    {
        printf ("ERROR = pigpio initialization failed");
        return 1;
    }

    // Check Serial module initialization
    serHandle = serOpen("/dev/ttyAMA1", 115200, 0); // ttyAMA1 = uart2
    if (serHandle < 0)
    {
        printf ("ERROR : Couldn't initialize UART : ");
        if (serHandle == PI_NO_HANDLE)
        {
            printf ("no handle available");
            return 1;
        }
        else if (serHandle == PI_SER_OPEN_FAILED)
        {
            printf (" couldn't open the interface");
            return 1;
        }
    }
    
    // Infinite loop
    while (1)
    {
        if (!printed)
        {
            serWrite(serHandle, "Hi from FRANCE\n",strlen("Hi from FRANCE\n"));
            printed = true;
        }
    }
    
    // Execution should never come here
    return 0;
}

/****************************************************/